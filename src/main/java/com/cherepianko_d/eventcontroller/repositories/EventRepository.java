package com.cherepianko_d.eventcontroller.repositories;

import com.cherepianko_d.eventcontroller.models.Event;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface EventRepository extends MongoRepository<Event, String> {

    void deleteByName(String name);
    Event findByName(String name);
}
