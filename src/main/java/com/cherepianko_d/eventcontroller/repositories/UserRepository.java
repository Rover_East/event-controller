package com.cherepianko_d.eventcontroller.repositories;

import com.cherepianko_d.eventcontroller.models.Event;
import com.cherepianko_d.eventcontroller.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User,String> {

    User getByNickName(String nickName);


}


