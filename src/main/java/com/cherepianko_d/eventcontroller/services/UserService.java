package com.cherepianko_d.eventcontroller.services;

import com.cherepianko_d.eventcontroller.models.Event;
import com.cherepianko_d.eventcontroller.models.User;
import com.cherepianko_d.eventcontroller.repositories.EventRepository;
import com.cherepianko_d.eventcontroller.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserService {

    private UserRepository userRepository;
    private EventRepository eventRepository;

    @Autowired
    public UserService(UserRepository userRepository, EventRepository eventRepository) {
        this.userRepository = userRepository;
        this.eventRepository = eventRepository;
    }

    public List<User> getAllUsers(String name){
        return userRepository.findAll();
    }

    public User getByNickname(String name){
        return userRepository.getByNickName(name);
    }

    public List<Event> getUserEvents(String name){
        return userRepository.getByNickName(name).getEventList();
    }

    public void addUser(User user){
        userRepository.save(user);
    }

    public void addEventToUser(String nickName, Event event){
        getUserEvents(nickName).add(event);
    }

}
