package com.cherepianko_d.eventcontroller.models;

import com.mongodb.client.model.geojson.Point;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class User {

    @Id
    private String id;

    private String nickName;
    private Point userPosition;
    private List<Event> eventList;

    public User() {
        //empty
    }

    public User(String nickName, Point userPosition) {
        this.nickName = nickName;
        this.userPosition = userPosition;
    }

    public User(String nickName, Point userPosition, List<Event> eventList) {
        this.nickName = nickName;
        this.userPosition = userPosition;
        this.eventList = eventList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Point getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(Point userPosition) {
        this.userPosition = userPosition;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", nickName='" + nickName + '\'' +
                ", userPosition=" + userPosition +
                ", eventList=" + eventList +
                '}';
    }
}
