package com.cherepianko_d.eventcontroller.models;

import com.mongodb.client.model.geojson.Point;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Event {

    @Id
    private String id;
    private String name;
    private Point eventPoint;
    private String eventTime;

    public Event() {
        //empty
    }

    public Event(String name, Point eventPoint, String eventTime) {
        this.name = name;
        this.eventPoint = eventPoint;
        this.eventTime = eventTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getEventPoint() {
        return eventPoint;
    }

    public void setEventPoint(Point eventPoint) {
        this.eventPoint = eventPoint;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", eventPoint=" + eventPoint +
                ", eventTime='" + eventTime + '\'' +
                '}';
    }
}
